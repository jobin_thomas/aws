package com.jthomas.catalogicsoftware.aws.models;

import java.util.Date;

public class VolumeAttachmentAWS {
	
	private Date attachmentTime;
	private boolean deleteOnTermination;
	private String device;
	private String instanceId;
	private String state;
	private String volumeId;
	public Date getAttachmentTime() {
		return attachmentTime;
	}
	public void setAttachmentTime(Date attachmentTime) {
		this.attachmentTime = attachmentTime;
	}
	public boolean isDeleteOnTermination() {
		return deleteOnTermination;
	}
	public void setDeleteOnTermination(boolean deleteOnTermination) {
		this.deleteOnTermination = deleteOnTermination;
	}
	public String getDevice() {
		return device;
	}
	public void setDevice(String device) {
		this.device = device;
	}
	public String getInstanceId() {
		return instanceId;
	}
	public void setInstanceId(String instanceId) {
		this.instanceId = instanceId;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getVolumeId() {
		return volumeId;
	}
	public void setVolumeId(String volumeId) {
		this.volumeId = volumeId;
	}

}
