package com.jthomas.catalogicsoftware.aws.models;

public class NetworkInterface {
	
	private String instanceNetworkInterfaceId;
	private String ownerId;
	private String status;
	private String vpcId;
	private String macAddress;
	private String description;
	private String subnetId;

	public String getInstanceNetworkInterfaceId() {
		return instanceNetworkInterfaceId;
	}

	public void setInstanceNetworkInterfaceId(String instanceNetworkInterfaceId) {
		this.instanceNetworkInterfaceId = instanceNetworkInterfaceId;
	}

	public String getOwnerId() {
		return ownerId;
	}

	public void setOwnerId(String ownerId) {
		this.ownerId = ownerId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getVpcId() {
		return vpcId;
	}

	public void setVpcId(String vpcId) {
		this.vpcId = vpcId;
	}

	public String getMacAddress() {
		return macAddress;
	}

	public void setMacAddress(String macAddress) {
		this.macAddress = macAddress;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getSubnetId() {
		return subnetId;
	}

	public void setSubnetId(String subnetId) {
		this.subnetId = subnetId;
	}

}
