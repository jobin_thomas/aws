package com.jthomas.catalogicsoftware.aws.models;

import java.util.Date;

public class EbsBlockDevice {
	
	private String status;
	
	private String volumeId;
	
	private Date attachedTime;
	
	private boolean deleteOnTermination;

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getVolumeId() {
		return volumeId;
	}

	public void setVolumeId(String volumeId) {
		this.volumeId = volumeId;
	}

	public Date getAttachedTime() {
		return attachedTime;
	}

	public void setAttachedTime(Date attachedTime) {
		this.attachedTime = attachedTime;
	}

	public boolean isDeleteOnTermination() {
		return deleteOnTermination;
	}

	public void setDeleteOnTermination(boolean deleteOnTermination) {
		this.deleteOnTermination = deleteOnTermination;
	}
}
