package com.jthomas.catalogicsoftware.aws.models;

public class BlockDeviceMapping {
	
	private String deviceName;
	
	private EbsBlockDevice ebsBlockDevice;
	

	public String getDeviceName() {
		return deviceName;
	}

	public void setDeviceName(String deviceName) {
		this.deviceName = deviceName;
	}

	public EbsBlockDevice getEbsBlockDevice() {
		return ebsBlockDevice;
	}

	public void setEbsBlockDevice(EbsBlockDevice ebsBlockDevice) {
		this.ebsBlockDevice = ebsBlockDevice;
	}

}
