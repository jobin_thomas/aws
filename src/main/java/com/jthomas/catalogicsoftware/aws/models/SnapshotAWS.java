package com.jthomas.catalogicsoftware.aws.models;

import java.util.Date;
import java.util.List;

import com.amazonaws.services.ec2.model.Tag;

public class SnapshotAWS {
	
	private String description;
	private String dataEncryptionKeyId;
	private boolean isEncrypted;
	private String kmsKeyId;
	private String ownerId;
	private String progress;
	private Date startTime;
	private String state;
	private String snapshotId;
	private String stateMessage;
	private List<TagAWS> tags;
	private String volumeId;
	private Integer volumeSize;
	
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getDataEncryptionKeyId() {
		return dataEncryptionKeyId;
	}
	public void setDataEncryptionKeyId(String dataEncryptionKeyId) {
		this.dataEncryptionKeyId = dataEncryptionKeyId;
	}
	public boolean isEncrypted() {
		return isEncrypted;
	}
	public void setEncrypted(boolean isEncrypted) {
		this.isEncrypted = isEncrypted;
	}
	public String getKmsKeyId() {
		return kmsKeyId;
	}
	public void setKmsKeyId(String kmsKeyId) {
		this.kmsKeyId = kmsKeyId;
	}
	public String getOwnerId() {
		return ownerId;
	}
	public void setOwnerId(String ownerId) {
		this.ownerId = ownerId;
	}
	public String getProgress() {
		return progress;
	}
	public void setProgress(String progress) {
		this.progress = progress;
	}
	public Date getStartTime() {
		return startTime;
	}
	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getSnapshotId() {
		return snapshotId;
	}
	public void setSnapshotId(String snapshotId) {
		this.snapshotId = snapshotId;
	}
	public String getStateMessage() {
		return stateMessage;
	}
	public void setStateMessage(String stateMessage) {
		this.stateMessage = stateMessage;
	}
	public List<TagAWS> getTags() {
		return tags;
	}
	public void setTags(List<TagAWS> tags) {
		this.tags = tags;
	}
	public String getVolumeId() {
		return volumeId;
	}
	public void setVolumeId(String volumeId) {
		this.volumeId = volumeId;
	}
	public Integer getVolumeSize() {
		return volumeSize;
	}
	public void setVolumeSize(Integer volumeSize) {
		this.volumeSize = volumeSize;
	}

}
