package com.jthomas.catalogicsoftware.aws.models;

import java.util.Date;
import java.util.List;

import com.jthomas.catalogicsoftware.aws.resource.Snapshot;

public class VolumeAWS {
	
	private List<VolumeAttachmentAWS> volumeAttachments;
	private String availabilityZone;
	private Date createTime;
	private boolean isEncrypted;
	private Integer iops;
	private String kmsKeyId;
	private Integer size;
	private String snapshotId;
	private Snapshot snapshotResource;
	private String state;
	private List<TagAWS> tags;
	private String volumeId;
	private String volumeType;
	
	public List<VolumeAttachmentAWS> getVolumeAttachments() {
		return volumeAttachments;
	}
	public void setVolumeAttachments(List<VolumeAttachmentAWS> volumeAttachments) {
		this.volumeAttachments = volumeAttachments;
	}
	public String getAvailabilityZone() {
		return availabilityZone;
	}
	public void setAvailabilityZone(String availabilityZone) {
		this.availabilityZone = availabilityZone;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public boolean isEncrypted() {
		return isEncrypted;
	}
	public void setEncrypted(boolean isEncrypted) {
		this.isEncrypted = isEncrypted;
	}
	public Integer getIops() {
		return iops;
	}
	public void setIops(Integer iops) {
		this.iops = iops;
	}
	public String getKmsKeyId() {
		return kmsKeyId;
	}
	public void setKmsKeyId(String kmsKeyId) {
		this.kmsKeyId = kmsKeyId;
	}
	public Integer getSize() {
		return size;
	}
	public void setSize(Integer size) {
		this.size = size;
	}
	public String getSnapshotId() {
		return snapshotId;
	}
	public void setSnapshotId(String snapshotId) {
		this.snapshotId = snapshotId;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public List<TagAWS> getTags() {
		return tags;
	}
	public void setTags(List<TagAWS> tags) {
		this.tags = tags;
	}
	public String getVolumeId() {
		return volumeId;
	}
	public void setVolumeId(String volumeId) {
		this.volumeId = volumeId;
	}
	public String getVolumeType() {
		return volumeType;
	}
	public void setVolumeType(String volumeType) {
		this.volumeType = volumeType;
	}
	public Snapshot getSnapshotResource() {
		return snapshotResource;
	}
	public void setSnapshotResource(Snapshot snapshotResource) {
		this.snapshotResource = snapshotResource;
	}
	
}
