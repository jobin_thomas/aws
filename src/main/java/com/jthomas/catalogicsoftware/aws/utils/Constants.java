package com.jthomas.catalogicsoftware.aws.utils;

public class Constants {
	
	public static final String FILTER_NAME_VOLUME_ID = "volume-id";
	public static final String FILTER_NAME_SNAPSHOT_ID = "snapshot-id";

}
