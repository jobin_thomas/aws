package com.jthomas.catalogicsoftware.aws.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.jthomas.catalogicsoftware.aws.models.EC2Instance;
import com.jthomas.catalogicsoftware.aws.models.SnapshotAWS;
import com.jthomas.catalogicsoftware.aws.models.VolumeAWS;
import com.jthomas.catalogicsoftware.aws.resource.AWSResource;
import com.jthomas.catalogicsoftware.aws.service.AWSService;
import com.jthomas.catalogicsoftware.aws.service.EC2Service;
import com.jthomas.catalogicsoftware.aws.service.SnapshotsService;
import com.jthomas.catalogicsoftware.aws.service.VolumesService;
import com.jthomas.catalogicsoftware.aws.utils.Constants;

@RestController
@RequestMapping("/aws")
public class AWSController {
	
	@Autowired
	private AWSService awsService;
	
	@Autowired
	private EC2Service ec2Service;
	
	@Autowired
	private SnapshotsService snapshotsService;
	
	@Autowired
	private VolumesService volumesService;
	
	@RequestMapping("")
	private AWSResource awsResources(){
		return awsService.getAWSResources();
	}
	
	@RequestMapping("/ec2-instances")
	public List<EC2Instance> ec2Instances(){
		return ec2Service.getEC2Instances();
	}
	
	@RequestMapping("/snapshots")
	public List<SnapshotAWS> snapshots(
			@RequestParam (value = Constants.FILTER_NAME_VOLUME_ID, required = false) String volumeId,
			@RequestParam (value = Constants.FILTER_NAME_SNAPSHOT_ID, required = false) String snapshotId){
		Map<String, String> filterNameValuesMap = new HashMap<String, String>();
		if(volumeId == null && snapshotId == null)
			return snapshotsService.getAllSnapshots();
		else if (volumeId != null && snapshotId != null) {
			filterNameValuesMap.put(Constants.FILTER_NAME_SNAPSHOT_ID, snapshotId);
			filterNameValuesMap.put(Constants.FILTER_NAME_VOLUME_ID, volumeId);
			return snapshotsService.getSnapshots(filterNameValuesMap);
		} else if(volumeId != null){
			filterNameValuesMap.put(Constants.FILTER_NAME_VOLUME_ID, volumeId);
			return snapshotsService.getSnapshots(filterNameValuesMap);
		}else{
			filterNameValuesMap.put(Constants.FILTER_NAME_SNAPSHOT_ID, snapshotId);
			return snapshotsService.getSnapshots(filterNameValuesMap);
		}
	}
	
	@RequestMapping("/volumes")
	public List<VolumeAWS> getVolumes(){
		return volumesService.getVolumes();
	}

}
