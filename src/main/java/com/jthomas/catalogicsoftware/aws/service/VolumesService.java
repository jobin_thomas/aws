package com.jthomas.catalogicsoftware.aws.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.amazonaws.services.ec2.AmazonEC2Client;
import com.amazonaws.services.ec2.model.DescribeVolumesResult;
import com.amazonaws.services.ec2.model.Tag;
import com.amazonaws.services.ec2.model.Volume;
import com.amazonaws.services.ec2.model.VolumeAttachment;
import com.jthomas.catalogicsoftware.aws.models.SnapshotAWS;
import com.jthomas.catalogicsoftware.aws.models.TagAWS;
import com.jthomas.catalogicsoftware.aws.models.VolumeAWS;
import com.jthomas.catalogicsoftware.aws.models.VolumeAttachmentAWS;
import com.jthomas.catalogicsoftware.aws.resource.builder.SnapshotResourceBuilder;

@Service
public class VolumesService {
	
	@Autowired
	private AmazonEC2Client amazonEC2Client;
	
	@Autowired
	private SnapshotResourceBuilder snapshotResourceBuilder;
	
	public List<VolumeAWS> getVolumes(){
		DescribeVolumesResult describeVolumes = amazonEC2Client.describeVolumes();
		List<VolumeAWS> volumes = new ArrayList<VolumeAWS>();
		for(Volume volume : describeVolumes.getVolumes()){
			VolumeAWS volumeAWS = new VolumeAWS();
			List<VolumeAttachmentAWS> volumeAttachments = new ArrayList<VolumeAttachmentAWS>();
			for(VolumeAttachment volAttachment : volume.getAttachments()){
				VolumeAttachmentAWS volAttachmentAWS = new VolumeAttachmentAWS();
				volAttachmentAWS.setAttachmentTime(volAttachment.getAttachTime());
				volAttachmentAWS.setDeleteOnTermination(volAttachment.getDeleteOnTermination());
				volAttachmentAWS.setDevice(volAttachment.getDevice());
				volAttachmentAWS.setInstanceId(volAttachment.getInstanceId());
				volAttachmentAWS.setState(volAttachment.getState());
				volAttachmentAWS.setVolumeId(volAttachment.getVolumeId());
				volumeAttachments.add(volAttachmentAWS);
			}
			volumeAWS.setVolumeAttachments(volumeAttachments);
			volumeAWS.setAvailabilityZone(volume.getAvailabilityZone());
			volumeAWS.setCreateTime(volume.getCreateTime());
			volumeAWS.setEncrypted(volume.getEncrypted());
			volumeAWS.setIops(volume.getIops());
			volumeAWS.setKmsKeyId(volume.getKmsKeyId());
			volumeAWS.setSize(volume.getSize());
			//volumeAWS.setSnapshotId(volume.getSnapshotId());
			SnapshotAWS snapshotAWS = new SnapshotAWS();
			snapshotAWS.setSnapshotId(volume.getSnapshotId());
			volumeAWS.setSnapshotResource(snapshotResourceBuilder.toResource(snapshotAWS));
			volumeAWS.setState(volume.getState());
			List<TagAWS> tagsVolumeAWS = new ArrayList<TagAWS>();
			for(Tag tag : volume.getTags()){
				TagAWS tagAWS = new TagAWS();
				tagAWS.setKey(tag.getKey());
				tagAWS.setValue(tag.getValue());
				tagsVolumeAWS.add(tagAWS);
			}
			volumeAWS.setTags(tagsVolumeAWS);
			volumeAWS.setVolumeId(volume.getVolumeId());
			volumeAWS.setVolumeType(volume.getVolumeType());
			volumes.add(volumeAWS);
		}
		return volumes;
	}

}
