package com.jthomas.catalogicsoftware.aws.service;

import org.springframework.hateoas.Link;
import org.springframework.hateoas.mvc.ControllerLinkBuilder;
import org.springframework.stereotype.Service;

import com.jthomas.catalogicsoftware.aws.controller.AWSController;
import com.jthomas.catalogicsoftware.aws.resource.AWSResource;
import com.jthomas.catalogicsoftware.aws.utils.Constants;

@Service
public class AWSService {
	
	public AWSResource getAWSResources(){
		AWSResource awsResource = new AWSResource();
		Link linkToEC2Instances = ControllerLinkBuilder.linkTo(AWSController.class).
				slash("/ec2-instances").withRel("ec2s");
		Link linkToSnapshots = ControllerLinkBuilder.linkTo(AWSController.class).
				slash("/snapshots?[" + Constants.FILTER_NAME_VOLUME_ID + ", " + 
		Constants.FILTER_NAME_SNAPSHOT_ID + "]").withRel("snapshots");
		Link linkToVolumes = ControllerLinkBuilder.linkTo(AWSController.class).
				slash("/volumes").withRel("volumes");
		Link[] links = new Link[]{linkToEC2Instances, linkToSnapshots, linkToVolumes};
		awsResource.add(links);
		return awsResource;
	}
	

}
