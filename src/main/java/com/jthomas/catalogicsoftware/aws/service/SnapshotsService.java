package com.jthomas.catalogicsoftware.aws.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.amazonaws.services.ec2.AmazonEC2Client;
import com.amazonaws.services.ec2.model.DescribeSnapshotsRequest;
import com.amazonaws.services.ec2.model.DescribeSnapshotsResult;
import com.amazonaws.services.ec2.model.Filter;
import com.amazonaws.services.ec2.model.Snapshot;
import com.amazonaws.services.ec2.model.Tag;
import com.jthomas.catalogicsoftware.aws.models.SnapshotAWS;
import com.jthomas.catalogicsoftware.aws.models.TagAWS;

@Service
public class SnapshotsService {
	
	@Autowired
	private AmazonEC2Client amazonEC2Client;
	
	public List<SnapshotAWS> getAllSnapshots(){
		DescribeSnapshotsResult describeSnapshotResults = amazonEC2Client.describeSnapshots();
		
		return getSnapshots(describeSnapshotResults);
	}
	
	public List<SnapshotAWS> getSnapshots(Map<String, String> nameValuesMap){
		DescribeSnapshotsRequest describeSnapshotRequest = new DescribeSnapshotsRequest();
		List<Filter> filters = new ArrayList<Filter>();
		for(Entry<String, String> entry : nameValuesMap.entrySet()){
			Filter filter = new Filter();
			filter.setName(entry.getKey());
			filter.setValues(Arrays.asList(entry.getValue()));
			filters.add(filter);
		}
		DescribeSnapshotsResult describeSnapshotsResult = 
				amazonEC2Client.describeSnapshots(describeSnapshotRequest.withFilters(filters));
		return getSnapshots(describeSnapshotsResult);
	}
	
	private List<SnapshotAWS> getSnapshots(DescribeSnapshotsResult describeSnapshotResults){
		List<Snapshot> snapshots = describeSnapshotResults.getSnapshots();
		List<SnapshotAWS> snapshotsAWS = new ArrayList<SnapshotAWS>();
		for(Snapshot snapshot : snapshots){
			SnapshotAWS snapshotAWS = new SnapshotAWS();
			snapshotAWS.setDescription(snapshot.getDescription());
			snapshotAWS.setDataEncryptionKeyId(snapshot.getDataEncryptionKeyId());
			snapshotAWS.setEncrypted(snapshot.getEncrypted());
			snapshotAWS.setKmsKeyId(snapshot.getKmsKeyId());
			snapshotAWS.setOwnerId(snapshot.getOwnerId());
			snapshotAWS.setProgress(snapshot.getProgress());
			snapshotAWS.setStartTime(snapshot.getStartTime());
			snapshotAWS.setState(snapshot.getState());
			snapshotAWS.setSnapshotId(snapshot.getSnapshotId());
			snapshotAWS.setStateMessage(snapshot.getStateMessage());
			List<Tag> tags = snapshot.getTags();
			List<TagAWS> tagsForSnapshots = new ArrayList<TagAWS>();
			for(Tag tag : tags){
				TagAWS tagSnapshotAWS = new TagAWS();
				tagSnapshotAWS.setKey(tag.getKey());
				tagSnapshotAWS.setValue(tag.getValue());
				tagsForSnapshots.add(tagSnapshotAWS);
 			}
			snapshotAWS.setTags(tagsForSnapshots);
			snapshotAWS.setVolumeId(snapshot.getVolumeId());
			snapshotAWS.setVolumeSize(snapshot.getVolumeSize());
			snapshotsAWS.add(snapshotAWS);
		}
		
		return snapshotsAWS;
	}

}
