package com.jthomas.catalogicsoftware.aws.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.ec2.AmazonEC2Client;
import com.amazonaws.services.ec2.model.DescribeInstancesResult;
import com.amazonaws.services.ec2.model.EbsInstanceBlockDevice;
import com.amazonaws.services.ec2.model.Instance;
import com.amazonaws.services.ec2.model.InstanceBlockDeviceMapping;
import com.amazonaws.services.ec2.model.InstanceNetworkInterface;
import com.amazonaws.services.ec2.model.InstanceState;
import com.amazonaws.services.ec2.model.Reservation;
import com.jthomas.catalogicsoftware.aws.models.BlockDeviceMapping;
import com.jthomas.catalogicsoftware.aws.models.EC2Instance;
import com.jthomas.catalogicsoftware.aws.models.EbsBlockDevice;
import com.jthomas.catalogicsoftware.aws.models.NetworkInterface;

@Service
public class EC2Service {
	
	@Autowired
	private AmazonEC2Client ec2Client;
	
	public List<EC2Instance> getEC2Instances() {
		List<EC2Instance> ec2Instances = new ArrayList<EC2Instance>();
		DescribeInstancesResult instancesResult = ec2Client.describeInstances();
		List<Reservation> reservations = instancesResult.getReservations();
		for(Reservation reservation : reservations){
			List<Instance> instances = reservation.getInstances();
			for(Instance instance : instances){
				EC2Instance ec2Instance = new EC2Instance();
				ec2Instance.setInstanceId(instance.getInstanceId());
				ec2Instance.setArchitecture(instance.getArchitecture());
				ec2Instance.setHypervisor(instance.getHypervisor());
				ec2Instance.setInstanceType(instance.getInstanceType());
				ec2Instance.setPublicDNSName(instance.getPublicDnsName());
				InstanceState instanceState = instance.getState();
				ec2Instance.setState(instanceState.getName());
				ec2Instance.setTags(instance.getTags());
				ec2Instance.setSubnetId(instance.getSubnetId());
				ec2Instance.setKeyName(instance.getKeyName());
				ec2Instance.setPublicIpAddress(instance.getPublicIpAddress());
				List<InstanceNetworkInterface> instanceNetworkInterfaces = instance.getNetworkInterfaces();
				List<NetworkInterface> networkInterfaces = new ArrayList<NetworkInterface>();
				for(InstanceNetworkInterface instanceNetworkInterface : instanceNetworkInterfaces){
					NetworkInterface networkInterface = new NetworkInterface();
					networkInterface.setInstanceNetworkInterfaceId(instanceNetworkInterface.getNetworkInterfaceId());
					networkInterface.setOwnerId(instanceNetworkInterface.getOwnerId());
					networkInterface.setStatus(instanceNetworkInterface.getStatus());
					networkInterface.setVpcId(instanceNetworkInterface.getVpcId());
					networkInterface.setMacAddress(instanceNetworkInterface.getMacAddress());
					networkInterface.setDescription(instanceNetworkInterface.getDescription());
					networkInterface.setSubnetId(instanceNetworkInterface.getSubnetId());
					networkInterfaces.add(networkInterface);
				}
				ec2Instance.setNetworkInterfaces(networkInterfaces);
				List<InstanceBlockDeviceMapping> blockDeviceMappings = 
						instance.getBlockDeviceMappings();
				List<BlockDeviceMapping> ebsDeviceMappings = new ArrayList<BlockDeviceMapping>();
				for(InstanceBlockDeviceMapping instanceBlockDeviceMapping : blockDeviceMappings){
					BlockDeviceMapping blockDeviceMapping = new BlockDeviceMapping();
					blockDeviceMapping.setDeviceName(instanceBlockDeviceMapping.getDeviceName());
					EbsInstanceBlockDevice ebsInstanceBlockDevice = instanceBlockDeviceMapping.getEbs();
					EbsBlockDevice ebsBlockDevice = new EbsBlockDevice();
					ebsBlockDevice.setVolumeId(ebsInstanceBlockDevice.getVolumeId());
					ebsBlockDevice.setStatus(ebsInstanceBlockDevice.getStatus());
					ebsBlockDevice.setAttachedTime(ebsInstanceBlockDevice.getAttachTime());
					ebsBlockDevice.setDeleteOnTermination(ebsInstanceBlockDevice.getDeleteOnTermination());
					blockDeviceMapping.setEbsBlockDevice(ebsBlockDevice);
					ec2Instance.setBlockDeviceMappings(ebsDeviceMappings);
				}
				ec2Instances.add(ec2Instance);
			}
		}
		return ec2Instances;
	}

}
