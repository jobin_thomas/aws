package com.jthomas.catalogicsoftware.aws.resource.builder;

import org.springframework.hateoas.Link;
import org.springframework.hateoas.mvc.ControllerLinkBuilder;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import org.springframework.stereotype.Component;

import com.jthomas.catalogicsoftware.aws.controller.AWSController;
import com.jthomas.catalogicsoftware.aws.models.SnapshotAWS;
import com.jthomas.catalogicsoftware.aws.resource.Snapshot;
import com.jthomas.catalogicsoftware.aws.utils.Constants;

@Component
public class SnapshotResourceBuilder extends 
	ResourceAssemblerSupport<SnapshotAWS, Snapshot>{

	public SnapshotResourceBuilder() {
		super(AWSController.class, Snapshot.class);
	}

	public Snapshot toResource(SnapshotAWS snapshotAWS) {
		Snapshot snapshotResource = new Snapshot();
		snapshotResource.setSnapshotId(snapshotAWS.getSnapshotId());
		
		ControllerLinkBuilder.linkTo(AWSController.class);
		Link linkToSnapshot = ControllerLinkBuilder.linkTo(AWSController.class)
				.slash("/snapshots?" + Constants.FILTER_NAME_SNAPSHOT_ID 
						+ "=" + snapshotAWS.getSnapshotId()).withSelfRel();
		snapshotResource.add(linkToSnapshot);
		return snapshotResource;
	}
	
	

}
