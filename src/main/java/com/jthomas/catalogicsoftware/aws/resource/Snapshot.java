package com.jthomas.catalogicsoftware.aws.resource;

import org.springframework.hateoas.ResourceSupport;

public class Snapshot extends ResourceSupport{
	
	private String snapshotId;

	public String getSnapshotId() {
		return snapshotId;
	}

	public void setSnapshotId(String snapshotId) {
		this.snapshotId = snapshotId;
	}
	

}
