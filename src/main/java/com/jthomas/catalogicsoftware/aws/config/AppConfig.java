package com.jthomas.catalogicsoftware.aws.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.ec2.AmazonEC2Client;

@Configuration
public class AppConfig {
	
	@Value("${awsAccessKey}")
	private String accessKey;
	
	@Value("${awsSecretKey}")
	private String secretKey;
	
	@Bean
	public AmazonEC2Client getAmazonEC2Client(){
		AWSCredentials awsCredentials = new BasicAWSCredentials(accessKey, secretKey);
		AmazonEC2Client amazonEC2Client = new AmazonEC2Client(awsCredentials);
		amazonEC2Client.setRegion(Region.getRegion(Regions.US_EAST_1));
		return amazonEC2Client;
	}

}
